# Biletado Pipelines
This repository contains gitlab-pipeline templates to include in other biletado projects.

## usage
In your projects `.gitlab-ci.yaml`:
```yaml
include:
  - project: 'biletado/pipeline'
    ref: "1"
    file: '/job-name.yml'
```

Replace the values of `ref` to the tag you want to include and `file` with the file of the job-name.
Includes should always use a tag as reference as this repository is not planned to have release-branches.

## use your own runner with podman
If you don't want to use the managed runners provided by GitLab because you don't want to share credit-card information,
or you think they are too slow, you can create your own runner with `docker` engine very easy with [podman](https://podman.io)

### 1. install dependencies
You will need `curl` and `jq` for this

#### Linux
```shell
# enterprise linux
sudo dnf install -y curl jq
# debian based
sudo apt install -y curl jq
```

#### Windows
```powershell
winget install -e --id jqlang.jq
winget install -e --id cURL.cURL
# reopen the terminal to register the new commands
```

### 2. create a personal access token
<!-- https://gitlab.com/gitlab-org/gitlab/-/issues/20087 -->
1. log into [GitLab](https://gitlab.com)
2. navigate to ["User Settings"/"Access Tokens"](https://gitlab.com/-/user_settings/personal_access_tokens)
3. click "Add new token"
4. fill the form with a name, an optional expiration date and at least the scope `create_runner`
5. save your token to some secret place

### 3. get you Project ID
Navigate to you project, the Project ID will be displayed below the project title with a copy button.

### 4. create the runner
#### Linux
```shell
# ensure the podman socket is enabled and running
systemctl enable --now --user podman.socket

# start the actual runner and pass the podman-socket
podman run -d \
    --name="gitlab-runner" \
    -v $XDG_RUNTIME_DIR/podman:/var/run/podman \
    -v gitlab-runner-config:/etc/gitlab-runner \
    --security-opt="label=disable" \
    registry.gitlab.com/gitlab-org/gitlab-runner:latest
```

#### Windows
```powershell
# start the actual runner and pass the podman-socket. (This needs the podman machine to be rootful.)
podman run -d `
    --name="gitlab-runner" `
    -v /var/run/podman:/var/run/podman `
    -v gitlab-runner-config:/etc/gitlab-runner `
    registry.gitlab.com/gitlab-org/gitlab-runner:latest
```

### 5. register and configure the runner for the project
#### Linux
```shell
# set the variables
export GL_PAT="glpat-YourToken123"; history -d $(history 1) # can be the same for each project
export GL_RUNNER_DESCRIPTION="podman-$HOSTNAME-$USER"

export GL_PROJECT_ID="1234"

# get a runner-token
export GL_RT=$(curl \
    --silent \
    --location \
    --request POST \
    --header "PRIVATE-TOKEN: $GL_PAT" \
    --data "runner_type=project_type" \
    --data "description=$GL_RUNNER_DESCRIPTION" \
    --data "run_untagged=true" \
    --data "tag_list=podman" \
    --data "project_id=$GL_PROJECT_ID" \
    "https://gitlab.com/api/v4/user/runners" \
    | jq --raw-output '.token')

# register the runner for the project
podman run --rm -it \
    -v gitlab-runner-config:/etc/gitlab-runner \
    registry.gitlab.com/gitlab-org/gitlab-runner:latest \
    register \
        --token="$GL_RT" \
        --url https://gitlab.com/ \
        --non-interactive \
        --docker-host="unix:///var/run/podman/podman.sock" \
        --docker-privileged \
        --executor="docker" \
        --docker-image="alpine:latest" \
        --description="$GL_RUNNER_DESCRIPTION"
```

#### Windows
```powershell
# set the variables
Set-PSReadlineOption -HistorySaveStyle SaveNothing
$GL_PAT = "glpat-YourToken123" # can be the same for each project
Set-PSReadlineOption -HistorySaveStyle SaveIncrementally # default
$GL_RUNNER_DESCRIPTION = "podman-$env:computername-$env:username"

# get a runner-token
$GL_RT=$(curl.exe `
    --silent `
    --location `
    --request POST `
    --header "PRIVATE-TOKEN: $GL_PAT" `
    --data "runner_type=project_type" `
    --data "description=$GL_RUNNER_DESCRIPTION" `
    --data "run_untagged=true" `
    --data "tag_list=podman" `
    --data "project_id=$GL_PROJECT_ID" `
    "https://gitlab.com/api/v4/user/runners" `
    | jq --raw-output '.token')

# register the runner for the project
podman run --rm -it `
    -v gitlab-runner-config:/etc/gitlab-runner `
    registry.gitlab.com/gitlab-org/gitlab-runner:latest `
    register `
        --token="$GL_RT" `
        --url https://gitlab.com/ `
        --non-interactive `
        --docker-host="unix:///var/run/podman/podman.sock" `
        --docker-privileged `
        --executor="docker" `
        --docker-image="alpine:latest" `
        --description="$GL_RUNNER_DESCRIPTION"
```

## jobs
### image-build

This job create a container-image for each commit with [buildah](https://buildah.io/)
and deployes it with alternative image-tags with [skopeo](https://github.com/containers/skopeo)

These images will be deployed with their CI_COMMIT_SHORT_SHA value (e.g. `1aa0416a`)
as well as their CI_COMMIT_REF_SLUG prefixed with `other-` (e.g. `other-feature-foobar` on branch `feature/foobar`)
or `latest` if on the default branch.
If the pipeline is triggered by a git tag, it will also use the git-tag as additional image-tag.
Please note, that in case of a tag the image is not getting rebuild.
If the image which was built for the commit of the tag already got deleted, e.g. by a cleanup job, make sure to run
the pipeline on that commit again.

#### variables
| name                  | default                   | description                                                                                            |
|-----------------------|---------------------------|--------------------------------------------------------------------------------------------------------|
| `CONTAINERFILENAME`   | `Containerfile`           | The name of the file containing the build-instructions. This was often named `Dockerfile` in the past. |
| `BUILDAH_PLATFORMS`   | `linux/amd64,linux/arm64` | The architectures the images shall be built for.                                                       |


#### cleanup-job
It is strongly recommended to [cleanup images tags](https://gitlab.com/help/user/packages/container_registry/reduce_container_registry_storage#cleanup-policy),
e.g. with the pattern `other-.*|[0-9a-f]{8}|[0-9a-f]{64}` with a daily run after 14 days of age.

### integration-tests

This job runs integration tests, based on [biletado/apidocs](https://gitlab.com/biletado/apidocs) which is based on 
[http-client-schema-check](https://gitlab.com/http-client-schema-check/http-client-schema-check) which is based on
IntelliJs HTTP Client and ajv, by setting up a temporary environment via [podman-kind](https://gitlab.com/podman-kind/podman-kind).

#### variables
| name                | default | description                                                                                    |
|---------------------|---------|------------------------------------------------------------------------------------------------|
| `ASSETS_VERSION`    | `v3`    | The version of the assets-definition which sall be tested againts.                             |
| `KUSTOMIZATION_DIR` | `.`     | The directory which contains the `kustomization.yaml` which shall be used for the environment. |
